FROM node:v8.12.0-alpine

COPY . /code
WORKDIR /code

RUN npm install

ENTRYPOINT node ./src/livepass.js
