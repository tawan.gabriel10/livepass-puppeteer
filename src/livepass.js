const fs = require('fs');
const puppeteer = require('puppeteer');
const request = require('request-promise');
const csv = require('csvtojson');

const readline = require('readline');
const { google } = require('googleapis');

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const TOKEN_PATH = 'token.json';

const defaultViewport = {
    deviceScaleFactor: 1,
    hasTouch: false,
    height: 1024,
    isLandscape: false,
    isMobile: false,
    width: 1280
};
  
const clickAndWait = async (page, selector, duration = 500) => {
    await page.click(selector)
    await page.waitFor(duration)
};

const sleepBot = async (page, duration = 500) => {
    await page.evaluate(async (duration) => {
        await new Promise(function (resolve) {
            setTimeout(resolve, duration);
        });
    }, duration);
};
  
const screenshot = 'livepass_results.png';

const file = fs.readFileSync('livepass.config');
const line = file.toString('utf8').split('\n');

const sheetId = line[0].split(/=(.+)/)[1];
let browser;

// Load client secrets from a local file.
fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Google Sheets API.
    authorize(JSON.parse(content), listMajors);
  });
  
  /**
   * Create an OAuth2 client with the given credentials, and then execute the
   * given callback function.
   * @param {Object} credentials The authorization client credentials.
   * @param {function} callback The callback to call with the authorized client.
   */
  function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);
  
    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
      if (err) return getNewToken(oAuth2Client, callback);
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client);
    });
  }
  
  /**
   * Get and store new token after prompting for user authorization, and then
   * execute the given callback with the authorized OAuth2 client.
   * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
   * @param {getEventsCallback} callback The callback for the authorized client.
   */
  function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
      rl.close();
      oAuth2Client.getToken(code, (err, token) => {
        if (err) return console.error('Error while trying to retrieve access token', err);
        oAuth2Client.setCredentials(token);
        // Store the token to disk for later program executions
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
          if (err) console.error(err);
          console.log('Token stored to', TOKEN_PATH);
        });
        callback(oAuth2Client);
      });
    });
  }


/**
 * Prints the names and majors of students in a sample spreadsheet:
 * @see https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function listMajors(auth) {
    const sheets = google.sheets({ version: 'v4', auth });
  
    sheets.spreadsheets.values.get({
      spreadsheetId: sheetId,
      range: 'parametros',
    }, (err, res) => {
      if (err) return console.log('The API returned an error: ' + err);
      const rows = res.data.values;
      let index = 2;
      if (rows.length) {
        rows.shift();
  
        try {
          (async () => {
            for (const row of rows) {
              console.log('Processando linha '+ (index - 1))
              const params = {
                        portal: row[0],
                        usuario: row[1],
                        senha: row[2],
                        evento: row[3],
                        local: row[4],
                        setor: row[5],
                        categoria: row[6],
                        quantidade: row[7],
                        bandeira: row[8],
                        cartao_numero: row[9],
                        cartao_nome: row[10],
                        cartao_expira_mes: row[12],
                        cartao_expira_ano: row[13],
                        cartao_cvc: row[14]
                    };

                    try {

                    browser = await puppeteer.launch({
                        headless: false, // launch headful mode
                        slowMo: 0, // slow down puppeteer script so that it's easier to follow visually
                        devtools: false,
                        ignoreHTTPSErrors: true,
                        args: [
                            '--no-sandbox',
                            '--disable-setuid-sandbox',
                            '--proxy-server="direct://"',
                            '--proxy-bypass-list=*',
                            '--disable-web-security',
                            '--lang=pt-BR,pt'
                        ]
                    });

                    try {
                    const page = await browser.newPage();
                    await page.setViewport(defaultViewport);
                    await page.goto(params.portal, {
                        timeout: 0
                    });

                    await page.type('#MainContent_ucLogin_txtLoginEmail', params.usuario)
                    await page.type('#MainContent_ucLogin_txtPassword', params.senha)
                    await clickAndWait(page, '#MainContent_ucLogin_btnLogin', 1000)

                    await page.goto('http://www.livepass.com.br/');

                    await page.type('input[name=s]', params.evento)
                    await clickAndWait(page, 'span.input-group-addon', 1000);

                    await page.waitForSelector('div.event-home');

                    await page.evaluate(async () => {
                        const eventoList = document.querySelectorAll('div.event-home');

                        eventoList[0].querySelector('div.event-box').querySelector('div.event-box-images')
                                .querySelector('div.white-mask').querySelector('a.link-icon').click();

                    });

                    await page.waitForNavigation({ waitUntil: 'networkidle2' });
                    await page.waitForSelector('div.view-dados');

                    await page.evaluate(async (params) => {
                        const localList = document.querySelectorAll('div.view-dados');
                        let urlLocal;
                        for (let i = 0; i < localList.length; i++) {
                            let local = localList[i].querySelector('div.view-dados-localizacao')
                                .querySelector('div.local-title')
                                .querySelector('p.event-name');

                            if (local.textContent.toUpperCase().trim() == params.local.toUpperCase()){
                                urlLocal = localList[i].querySelector('div.view-dados-localizacao')
                                                .querySelector('div.button-tickets-info')
                                                .querySelector('div.text-center')
                                                .querySelector('a.button-buy-icon');
                                break;
                            }
                        }
                        if (urlLocal) {
                            urlLocal.click();
                            return;
                        }
                    }, params);
                    
                    await page.waitForNavigation({ waitUntil: 'networkidle2' });

                    await clickAndWait(page, '#ctl00_MainContent_rptPromotions_ctl00_boxPromotion_cmdNext2', 2000);

                    await page.waitForSelector('div.panel-default');
                
                    await page.evaluate(async (params) => {
                        const setorList = document.querySelectorAll('div.panel-default');
                        
                        for (let i = 0; i < setorList.length; i++) {
                            var setor = setorList[i].querySelector('div.panel-heading')
                                                .querySelector('span').textContent;
                        
                            if (setor.toUpperCase().trim() == params.setor.toUpperCase()) {
                                var categoriaList = setorList[i].querySelector('div.panel-body')
                                                        .querySelector('table.table-condensed')
                                                        .querySelector('tbody')
                                                        .querySelectorAll('tr');
                                for (let j = 0; j < categoriaList.length; j++) {
                                    let iscategoria = false;
                                    var tds = categoriaList[j].querySelectorAll('td');
                                    for (let k = 0; k < tds.length; k++) {
                                        if (tds[k].textContent.toUpperCase().trim()) {
                                            iscategoria  = true;
                                        }
                                    }

                                    if (iscategoria) {
                                        categoriaList[j].querySelector('input.form-control').value = params.quantidade;
                                        break;
                                    }
                                }
                            }
                        }
                    }, params);

                    await sleepBot(page, 2000);
                    await page.waitForSelector('#recaptcha1');

                    await clickAndWait(page, '#recaptcha1', 5000);
                
                    await page.evaluate(async () => {
                        let resp = true;
                        while (resp) {

                            await new Promise(function (resolve) {
                                setTimeout(resolve, 5000);
                            });
                            
                            resp = !confirm("Já concluiu o Recaptcha?");

                            let recaptcha = document.querySelector(`#g-recaptcha-response`).value;

                            alert(recaptcha.toString());

                        }
                    });


                    await clickAndWait(page, '#MainContent_cmdAddToBasket', 1000);

                    await page.waitForNavigation({ waitUntil: 'networkidle2' });

                    await clickAndWait(page, '#MainContent_cmdNext', 1000);

                    await page.waitForNavigation({ waitUntil: 'networkidle2' });

                    await page.evaluate(async (params) => {
                        const entregaList = document.querySelectorAll('#deliveryMethodTypes li');
                        
                        for (let i = 0; i < entregaList.length; i++) {
                            let entrega = entregaList[i].querySelector('div.delivery-type-item')
                                                        .querySelector('label').textContent;
                            if (entrega.trim() == 'Imprimir em casa | easyPASS home' || entrega.trim() == 'Retirar na bilheteria ou pontos de retirada') {
                                entregaList[i].querySelector('div.delivery-type-item')
                                    .querySelector('input[type=radio]').click();
                                break;
                            }
                        }
                    },params);

                    await sleepBot(page, 1000);
                    await page.waitForSelector('ul.list-payment-method-types');

                    await page.evaluate(async (params) => {
                        const bandeiraList = document.querySelectorAll('li.js-payment-method-type');

                        let attrData;
                        for (let i = 0; i < bandeiraList.length; i++) {
                            let bandeira = bandeiraList[i].querySelector('div').querySelector('img').getAttribute('alt');
                            if (bandeira.toUpperCase() == params.bandeira.toUpperCase()) {
                                attrData = bandeiraList[i].getAttribute('data-typeid');
                                bandeiraList[i].querySelector('div').querySelector('img').click();
                                break;
                            }
                        }

                        if (attrData) {
                            const parcelasList = document.querySelector('#type_' + attrData).querySelectorAll('div.pmoption');
                            for (let j = 0; j < parcelasList.length; j++) {
                                parcelasList[j].click();
                                break;
                            }
                        }
                    }, params);
                    
                    await sleepBot(page, 2000);

                    await clickAndWait(page, '#cmdNext', 1000);

                    await page.waitForNavigation({ waitUntil: 'networkidle2' });
                    await page.waitForSelector('ul.list-payment-method-types');

                    await page.type('#creditCardNumber', params.evento);
                    await page.type('#creditCardHolderName', params.evento);
                    await page.type('#creditCardExpirationMonth', params.evento);
                    await page.type('#creditCardExpirationYear', params.evento);
                    await page.type('#creditCardSecurityCode', params.evento);
                    
                    await sleepBot(page, 1000);

                    await clickAndWait(page, 'a.dropdown-toggle', 1000);

                    await clickAndWait(page, 'a.logout', 2000);
                  
                    await page.screenshot({ path: screenshot })

                    await browser.close()
                    console.log('See screenshot: ' + screenshot)

                    let values = [
                      row
                    ]
    
                    sheets.spreadsheets.values.append({
                      spreadsheetId: sheetId,
                      range: 'vendas processadas!A' + index,
                      valueInputOption: 'USER_ENTERED',
                      resource: { values },
                    }, function (err, response) {
                      // console.log(err)
                      // console.log(response)
                    });
    
                    sheets.spreadsheets.values.clear({
                      spreadsheetId: sheetId,
                      range: 'parametros!A' + index + ':T' + index,
                      //resource: {values},
                    }, function (err, response) {
                      // console.log(err)
                      // console.log(response)
                    });
    
                    index++;
    
                  } catch (err) {
                    console.error(err)
                    index++;
                    await browser.close()
                  }
    
                } catch (err) {
                  console.error(err)
                }
              }
            })();
          } catch (err) {
            console.error(err)
          }
        } else {
          console.log('No data found.');
        }
      });
    }